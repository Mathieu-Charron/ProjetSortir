let outingLocation;

window.addEventListener("load",function () {
    outingLocation = document.querySelector("#outing_location");
    if(outingLocation) locationData();
    outingLocation?.addEventListener("change", locationData);

    const formLocation = document.querySelector("template").content.querySelector("form[name=location]");
    document.querySelector("#addLocation").addEventListener("click", () => new ModalForm(formLocation, addNewLocationToSelect, "Ajout d'un lieu").init())

});

const locationData = () =>{

    fetch(path+"api/location/find/"+outingLocation.value, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => response.json())
        .then(location => {
            document.querySelector("#street").innerText = location.street;
            document.querySelector("#postalCode").innerText = location.postal_code;
            document.querySelector("#longitude").innerText = location.longitude;
            document.querySelector("#latitude").innerText = location.latitude;
        });
}


const addNewLocationToSelect = (location) =>{
    const selectLocation =  document.querySelector("#outing_location");
    const newOption = utils.createElementWithParams("option", location.name, {"value": location.id, "selected": ""});
    selectLocation.appendChild(newOption);
    selectLocation.dispatchEvent(new Event("change"));
}


class ModalForm extends Modal{
    /**
     *
     * @param form {HTMLFormElement}
     * @param size {string}
     * @param onValidation {Function}
     * @param title {string}
     */
    constructor(form,onValidation, title = '',size = "xl") {
        super(size);
        this.form = form.cloneNode(true);
        this.onValidation = onValidation;
        this.title = title;
    }

    init = () =>{
        this.initModal();
        this.initForm();
    }

    initForm = () =>{
        this.modalContent.appendChild(utils.createElementWithParams('H1',this.title))
        this.modalContent.appendChild(this.form);
        const handleResponse = this.handleResponse;
        const route = this.form.action;


        this.form.addEventListener('submit', function(event) {
            event.preventDefault();
            // console.log(new FormData(this));

            fetch(route, {
                method: 'POST',
                body: new FormData(this),
            })
                .then(response => response.json())
                .then(json => {
                    handleResponse(json);
                });

        });
    }

    handleResponse = (response) => {
        this.removeErrors();
        switch(response.code) {
            case 'valide':
                this.executeValidationAndDeleteModal(response.object);
                break;
            case 'invalide':
                this.handleErrors(response.errors);
                break;
        }
    }

    removeErrors = () => {
        const invalidFeedbackElements = document.querySelectorAll('.invalid-feedback');
        const isInvalidElements = document.querySelectorAll('.is-invalid');

        invalidFeedbackElements.forEach(errorElement => errorElement.remove());
        isInvalidElements.forEach(isInvalidElement => isInvalidElement.classList.remove('is-invalid'));
    }


    handleErrors = (errors) => {
        if (errors.length === 0) return;

        for (const key in errors) {
            let element = document.querySelector(`#location_${key}`);
            element.classList.add('is-invalid');

            let div = document.createElement('div');
            div.classList.add('invalid-feedback', 'd-block');
            div.innerText = errors[key];

            element.after(div);
        }
    }

    executeValidationAndDeleteModal = (data) =>{
        this.onValidation(data);
        this.deleteModal();
    }

}