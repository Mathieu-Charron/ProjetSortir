let nameContainInput, siteIdSelect, iAmOrganizerCheckBox, iAmRegisteredCheckbox, iAmNotRegisteredCheckbox, isOutingPastCheckbox, startDateInput, endDateInput;
let tBodyOuting;

window.addEventListener("load",function (){
    tBodyOuting = document.querySelector(".table-outing tbody");

    nameContainInput = document.querySelector("#outingName");
    siteIdSelect = document.querySelector("#site-select");
    iAmOrganizerCheckBox = document.querySelector("#iAmOrganizerCheckBox");
    iAmRegisteredCheckbox = document.querySelector("#iAmRegisteredCheckbox");
    iAmNotRegisteredCheckbox = document.querySelector("#iAmNotRegisteredCheckbox");
    isOutingPastCheckbox = document.querySelector("#isOutingPastCheckbox");
    startDateInput = document.querySelector("#startDateInput");
    endDateInput = document.querySelector("#endDateInput");

    loadTable();
    nameContainInput.addEventListener("input", loadTable);
    siteIdSelect.addEventListener("change", loadTable);
    iAmOrganizerCheckBox.addEventListener("change", loadTable);
    iAmRegisteredCheckbox.addEventListener("change", loadTable);
    iAmNotRegisteredCheckbox.addEventListener("change", loadTable);
    isOutingPastCheckbox.addEventListener("change", loadTable);
    startDateInput.addEventListener("change", loadTable);
    endDateInput.addEventListener("change", loadTable);
});



loadTable = () => {

    const params = setParams();

    fetch(path+"api/outing/dataSearch?"+params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => response.json())
        .then(items => {
            tBodyOuting.innerHTML = "";
            for(const item of items){
                new tableRowOuting(item);
            }
        });
}

function setParams(){
    let params = "";
    const siteId = siteIdSelect.value;
    params+= "siteId="+siteId+"&";

    const nameContain = nameContainInput.value;
    params+= "nameContain="+nameContain+"&";

    const iAmOrganizer = iAmOrganizerCheckBox.checked;
    params+= "iAmOrganizer="+iAmOrganizer+"&";

    const iAmRegistered = iAmRegisteredCheckbox.checked;
    params+= "iAmRegistered="+iAmRegistered+"&";

    const iAmNotRegistered = iAmNotRegisteredCheckbox.checked;
    params+= "iAmNotRegistered="+iAmNotRegistered+"&";

    const isOutingPast = isOutingPastCheckbox.checked;
    params+= "isOutingPast="+isOutingPast+"&";

    const startDate = startDateInput.value;
    params+= "startDate="+startDate+"&";

    const endDate = endDateInput.value;
    params+= "endDate="+endDate+"&";

    return params;
}

class tableRowOuting{
    constructor(outing) {
        this.outing = outing;
        this.initRow();
        this.initColumnOutingName();
        this.initColumnOutingStartDate();
        this.initColumnOutingClosingDate();
        this.initColumnRegistered();
        this.initColumnState();
        this.initColumnIsRegistered();
        this.initColumnOrganizer();
        this.initColumnActionModifOrConsulting();
        this.initColumnActionUnregisterOrRegisterOrOpenOrDelete();
    }
    initRow = () =>{
        this.tr = document.createElement("TR");
        tBodyOuting.appendChild(this.tr);
    }

    initColumnOutingName = () =>{
        this.tdOutingName = utils.createElementWithParams("td", this.outing.name,{"style": "font-weight: bold"});
        this.tr.appendChild(this.tdOutingName);
    }

    initColumnOutingStartDate = () =>{
        this.tdOutingStartDate = utils.createElementWithParams("TD", utils.formatDatetimeStringToDatetimeStringFr( this.outing.start_date));
        this.tr.appendChild(this.tdOutingStartDate);
    };

    initColumnOutingClosingDate = () =>{
        this.tdOutingClosingDate = utils.createElementWithParams("TD", utils.formatDateStringToDateStringFr(this.outing.closing_date));
        this.tr.appendChild(this.tdOutingClosingDate);
    };

    initColumnRegistered = () =>{
        this.tdRegistered = utils.createElementWithParams("TD",this.outing.count_participant+"/"+this.outing.max_registrations)
        this.tr.appendChild(this.tdRegistered);
    }

    initColumnState = () =>{
        this.tdState = utils.createElementWithParams("TD", this.outing.state);
        this.tr.appendChild(this.tdState);

    }

    initColumnIsRegistered = () =>{

        this.tdIsRegistered = utils.createElementWithParams("TD");

        if(this.outing.isRegistered){
            const img = utils.createElementWithParams("img", '',{"src": path+"img/icon/cross.svg"}, ["icon"]);
            this.tdIsRegistered.appendChild(img);
        }
        this.tr.appendChild(this.tdIsRegistered);
    }

    initColumnOrganizer = () =>{
        this.tdOrganizer = utils.createElementWithParams("TD");
        const linkElement = utils.createElementWithParams("a", this.outing.organizer_name, {"href": path+"profile/"+this.outing.organizer_id}, ["link-profile"]);
        this.tdOrganizer.appendChild(linkElement);
        this.tr.appendChild(this.tdOrganizer);
    }

    initColumnActionModifOrConsulting = () =>{
        this.tdColumnActionModifOrConsulting = utils.createElementWithParams("TD");

        const isModeEdit = this.outing.organizer_id == userId && this.outing.state == "Créée";

        const route = isModeEdit ? "edit" : "detail";

        const linkElement = utils.createElementWithParams("a", '', {"href": path+"outing/"+route+"/"+this.outing.id})
        const img = utils.createElementWithParams("img", '',{"src": path+"img/icon/"+route+".svg"}, ["icon"]);
        linkElement.appendChild(img);
        this.tdColumnActionModifOrConsulting.appendChild(linkElement);
        this.tr.appendChild(this.tdColumnActionModifOrConsulting);
    }
    initColumnActionUnregisterOrRegisterOrOpenOrDelete = () =>{
        this.tdActionDeleteOrRegisterOrOpenDelete = utils.createElementWithParams("TD");


        let icon, action, message, modal;
        if(this.outing.state !== "Passée" && this.outing.state !== "En cours" && !(!this.outing.isRegistered && this.outing.state === "Clôturée")){
            if(this.outing.isRegistered){
                if(this.outing.organizer_id == userId){
                    if(this.outing.state ==="Créée"){
                        icon = "publish"
                        action = this.#actionPublish;
                        message = "Voulez vous vraiment publier la sortie ?";
                        modal = ModalConfirm;
                    }else if(this.outing.state === "Ouverte" ||this.outing.state === "Clôturée"){
                        icon = "delete";
                        action = this.#actionCancel;
                        message = "Voulez vous vraiment annuler la sortie ?";
                        modal = ModalConfirmWithReason;
                    }
                }else{
                    icon = "unregistered";
                    action = this.#actionUnregister;
                    message = "Voulez vous vraiment vous désinscrire de la sortie ?";
                    modal = ModalConfirm;
                }
            }else{
                icon = "registered";
                action = this.#actionRegister;
                message = "Voulez vous vraiment vous inscrire à la sortie ?";
                modal = ModalConfirm;
                // message = "Voulez vous vraiment vous inscrire à la sortie \""+this.outing.name+"\" ?";
            }
            const img = utils.createElementWithParams("img", '',{"src": path+"img/icon/"+icon+".svg"}, ["icon"]);
            img.addEventListener("click", () => new modal(message, action).init());
            this.tdActionDeleteOrRegisterOrOpenDelete.appendChild(img);
        }



        this.tr.appendChild(this.tdActionDeleteOrRegisterOrOpenDelete);
    }

    #actionRegister = () => this.#genericAction("register");
    #actionUnregister = () => this.#genericAction("unregister");
    #actionPublish = () => this.#genericAction("publish");

    #actionCancel = () => {
        const reason = document.querySelector("#reason-modal") .value;

        fetch(path + "api/outing/cancel/" + this.outing.id+"/"+reason, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(r => loadTable());
    }

    #genericAction = (action) => {
        fetch(path + "api/outing/"+action+"/" + this.outing.id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {

            if (!response.ok) {
                // throw new Error(response);
                response.json().then(res => {
                    new ModalError(res).init();
                })
            }
            return response.json();
        }).then(data => {
            // Traitement des données si la requête réussit
            loadTable();
        })
        //     .catch((error) => {
        //     // error.json().then(res =>  console.log(res))
        //     // response.json().then(jsonError => {
        //     //     console.log('Message d\'erreur du serveur:', jsonError.message);
        //     // })
        //     // console.log( error.message)
        // });
    }
}


/** MODAL ***/

class ModalConfirm extends Modal{
    constructor(message = "Voulez vous valider l'action ?", actionOnYes, actionOnNo, size) {
        super(size);
        if(!actionOnNo){
            actionOnNo = this.deleteModal;
        }
        this.actionOnNo = actionOnNo;
        this.actionOnYes = actionOnYes;
        this.message = message;
        // this.init();
    }

    init = () =>{
        this.initModal();
        this.initContent();
        this.initButtons();
    };


    initContent = () =>{
        this.textElement = utils.createElementWithParams("p", this.message, {}, ["text-modal"]);
        this.modalContent.appendChild(this.textElement);
    }

    initButtons = () => {
        const buttonContainer = utils.createElementWithParams("div", "", {}, ["buttonContainer"])
        this.buttonYes = utils.createElementWithParams("button", "OUI", {"id": "btnYes"}, ["acceptButton"]);
        this.buttonYes.addEventListener("click", () => this.actionOnYesPlusDeleteModal(this.actionOnYes));
        const buttonNo = utils.createElementWithParams("button", "NON", {"id": "btnNo"}, ["declineButton"]);
        buttonNo.addEventListener("click", this.actionOnNo);
        buttonContainer.appendChild(this.buttonYes);
        buttonContainer.appendChild(buttonNo);
        this.modalContent.appendChild(buttonContainer);

    }

    actionOnYesPlusDeleteModal = () => {
        this.actionOnYes();
        this.deleteModal();
    };

}

class ModalConfirmWithReason extends ModalConfirm{
    constructor(message = "Voulez vous valider l'action ?", actionOnYes, actionOnNo, size) {
        super(message, actionOnYes, actionOnNo, size);
    }

    init = () =>{
        this.initModal();
        this.initContentWithReason();
        this.initButtons();
    }

    initContentWithReason = () =>{
        this.initContent();
        this.textAreaReason = utils.createElementWithParams("textarea", "", {"id": "reason-modal", "placeholder": "raison de confirmation"});
        this.textElement.insertAdjacentElement("afterend", this.textAreaReason);

    }

    actionOnYesPlusDeleteModal = () => {
        if(this.textAreaReason.value){
            this.actionOnYes();
            this.deleteModal();
        }else{
            this.reasonErrorMessage = utils.createElementWithParams("p", "Veuillez renseigner la raison d'annulation",{}, ["color-error"]);
            this.textAreaReason.insertAdjacentElement("afterend", this.reasonErrorMessage);
        }

    };
}

class ModalError extends Modal {
    constructor(errorMessage) {
        super("xs");
        this.errorMessage = errorMessage;
    }

    init = () => {
        super.initModal();
        this.initErrorMessage();
    }

    initErrorMessage(){
        this.textElement = utils.createElementWithParams("p", this.errorMessage, {}, ["text-modal", "color-error"]);
        this.modalContent.appendChild(this.textElement);
    }
}
// <div id="modal-generic" className="modal">
//     <div id="modal-content" className="modal-content">
//         <span className="close" id="closeModal">&times;</span>
//         <p className="text-modal">Es tu sur de vouloir faire cette action ?</p>
//         <div className="buttonContainer">
//             <button id="btnYes" className="acceptButton">OUI</button>
//             <button id="btnNo" className="declineButton">NON</button>
//         </div>
//     </div>
// </div>

