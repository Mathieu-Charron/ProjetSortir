<?php

namespace App\Repository;

use App\Entity\Outing;
use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use http\Client\Curl\User;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @extends ServiceEntityRepository<Outing>
 *
 * @method Outing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Outing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Outing[]    findAll()
 * @method Outing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutingRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Outing::class);
    }

    public function searchOuting(   int $userIdAuthenticate,
                                    int $siteId = null,
                                    string $nameContain = null,
                                    \DateTime $startDate = null,
                                    \DateTime $endDate = null,
                                    bool $iAmOrganizer = false,
                                    bool $iAmRegistered = false,
                                    bool $iAmNotRegistered = false,
                                    bool $isOutingPast = false
                                ){
        $conn = $this->getEntityManager()->getConnection();

        $now = new \DateTime();
        $todayMinus30Days = $now->modify('-30 days')->format('Y-m-d');
        $now = $now->format('Y-m-d H:i:s');


        $startDate = $startDate ?? \DateTime::createFromFormat('Y-m-d H:i:s', '0001-01-01 00:00:00');
        $endDate = $endDate ?? \DateTime::createFromFormat('Y-m-d H:i:s', '9999-01-01 00:00:00');

        $startDate = $startDate->format('Y-m-d');
        $endDate = $endDate->format('Y-m-d');
//        $query = $this->createQueryBuilder('o');
//        $query =

        $query = '
            SELECT o.id,
                   o.name,
                   o.start_date,
                   o.closing_date,
                   (SELECT count(*) FROM outing_participant op where op.outing_id = o.id) AS count_participant,
                   o.max_registrations,
                   s.name AS state,
                   CASE WHEN (SELECT count(*) FROM outing_participant op where op.outing_id = o.id AND op.participant_id = :userIdAuthenticate) > 0
                       THEN 1 ELSE 0 
                    END AS isRegistered,
                   organizer.username AS organizer_name,
                   organizer.id AS organizer_id
            FROM outing o
            JOIN state s on s.id = o.state_id
            JOIN participant organizer on organizer.id = o.organizer_id
            WHERE o.start_date BETWEEN :start_date AND :end_date
            AND o.start_date > :todayMinus30Days
            AND (s.name NOT IN (:stateCancel, :stateCreate)
            OR (s.name = :stateCreate AND o.organizer_id = :userIdAuthenticate))
            ';

        $params = [
            'start_date'=>$startDate,
            'end_date'=>$endDate,
            'todayMinus30Days' => $todayMinus30Days,
            'stateCancel' => StateRepository::STATE_CANCEL,
            'stateCreate' => StateRepository::STATE_CREATE,
            'userIdAuthenticate' => $userIdAuthenticate
        ];

        if($siteId != null){
            $params["siteId"] = $siteId;
            $query.= ' AND o.site_id = :siteId';
        }

        if($nameContain != null){
            $params["nameContain"] = '%'.$nameContain.'%';
            $query.= ' AND o.name LIKE :nameContain';
        }

        if($iAmOrganizer){
            $query.= ' AND organizer.id = :userIdAuthenticate';
        }
        if($iAmRegistered){
            $query.= ' AND EXISTS (SELECT op.participant_id FROM outing_participant op where op.outing_id = o.id AND op.participant_id = :userIdAuthenticate)';
        }

        if($iAmNotRegistered){
            $query.= ' AND NOT EXISTS (SELECT op.participant_id FROM outing_participant op where op.outing_id = o.id AND op.participant_id = :userIdAuthenticate)';
        }

        if($isOutingPast){
            $params["statePast"] = StateRepository::STATE_PAST;
            $query.= ' AND o.start_date < now()
                        AND s.name = :statePast';
        }

//        JOIN participant p ON p.id = o.organizer_id


//        return $query->getQuery()->getResult();
        return $conn->executeQuery($query, $params)->fetchAllAssociative();
//        $query = $manager->createQuery('SELECT p FROM ProjetSortir\Model\Participant p');
    }

//    /**
//     * @return Outing[] Returns an array of Outing objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Outing
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
