<?php

namespace App\Repository;

use App\Entity\OutingPhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OutingPhoto>
 *
 * @method OutingPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutingPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutingPhoto[]    findAll()
 * @method OutingPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutingPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutingPhoto::class);
    }

//    /**
//     * @return OutingPhoto[] Returns an array of OutingPhoto objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?OutingPhoto
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
