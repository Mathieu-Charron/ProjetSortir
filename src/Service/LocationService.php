<?php

namespace App\Service;

use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationService
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {}

    public function handleFormData(FormInterface $locationForm): JsonResponse
    {
        if ($locationForm->isValid()) {
            return $this->handleValidForm($locationForm);
        } else {
            return $this->handleInvalidForm($locationForm);
        }
    }

    private function handleValidForm(FormInterface $locationForm) : JsonResponse
    {
        /** @var Location $location */
        $location = $locationForm->getData();


        $this->em->persist($location);
        $this->em->flush();

        return new JsonResponse([
            'code' => "valide",
            'object' => ["id" => $location->getId(), "name" => $location->getName()]
            ]);
    }

    private function handleInvalidForm(FormInterface $locationForm) : JsonResponse
    {
        return new JsonResponse([
            'code' => "invalide",
            'errors' => $this->getErrorMessages($locationForm)
        ]);
    }


    private function getErrorMessages(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}