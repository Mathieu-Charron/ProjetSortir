<?php
namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UnauthorizedAccessException extends HttpException
{
    public function __construct(string $message = "", \Exception $previous = null, ?int $code = 0, array $headers = [])
    {
        parent::__construct(401, $message, $previous, $headers, $code);
    }
}