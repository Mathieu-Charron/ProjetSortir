<?php

namespace App\Entity;

use App\Repository\OutingRepository;
use App\Repository\StateRepository;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: OutingRepository::class)]
class Outing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotNull(message: "Le nom de la sortie ne doit pas être vide")]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotNull(message: "La date de la sortie ne doit pas être vide")]
    #[Assert\GreaterThan(value: "today", message: "La date de la sortie doit être après la date actuelle.")]
    private ?\DateTimeInterface $startDate = null;

    #[ORM\Column(nullable: true)]
    #[Assert\Positive(message: "La durée doit être supérieur à zéro")]
    private ?int $duration = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\NotNull(message: "La date limite d'inscription ne doit pas être vide")]
    #[Assert\LessThan(propertyPath: "startDate", message: "La date limite d'inscription doit être avant la date de la sortie.")]
    #[Assert\GreaterThan(value: "today", message: "La date limite d'inscription doit être après la date actuelle.")]
    private ?\DateTimeInterface $closingDate = null;

    #[ORM\Column]
    #[Assert\NotNull(message: "Le nombre de places ne doit pas être vide")]
    #[Assert\GreaterThan(1, message: "Le nombre de places doit être supérieur à 1")]
    #[Assert\Type(type: "integer" ,message: "Le nombre de place doit être un nombre entier")]
    private ?int $maxRegistrations = null;


    #[ORM\Column(length: 500, nullable: true)]
    #[Assert\Length(max: 500, maxMessage: "la description ne peut pas excéder 500 caractères")]
    private ?string $infoDescription = null;

    #[ORM\ManyToOne(inversedBy: 'outings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?State $state = null;


    #[ORM\ManyToOne(inversedBy: 'outings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Location $location = null;

    #[ORM\OneToMany(mappedBy: 'outing', targetEntity: OutingPhoto::class, orphanRemoval: true)]
    private Collection $outingPhotos;

    #[ORM\ManyToMany(targetEntity: Participant::class, inversedBy: 'outings')]
    private Collection $participants;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Participant $organizer = null;

    #[ORM\ManyToOne(inversedBy: 'outings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Site $site = null;

    #[ORM\Column(length: 500, nullable: true)]
    private ?string $cancelReason = null;


    public function __construct()
    {
        $this->outingPhotos = new ArrayCollection();
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): static
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): static
    {
        $this->duration = $duration;

        return $this;
    }

    public function getClosingDate(): ?\DateTimeInterface
    {
        return $this->closingDate;
    }

    public function setClosingDate(?\DateTimeInterface $closingDate): static
    {
        $this->closingDate = $closingDate;

        return $this;
    }

    public function getMaxRegistrations(): ?int
    {
        return $this->maxRegistrations;
    }

    public function setMaxRegistrations(int $maxRegistrations): static
    {
        $this->maxRegistrations = $maxRegistrations;

        return $this;
    }

    public function getInfoDescription(): ?string
    {
        return $this->infoDescription;
    }

    public function setInfoDescription(?string $infoDescription): static
    {
        $this->infoDescription = $infoDescription;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): static
    {
        $this->state = $state;

        return $this;
    }


    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): static
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection<int, OutingPhoto>
     */
    public function getOutingPhotos(): Collection
    {
        return $this->outingPhotos;
    }

    public function addOutingPhoto(OutingPhoto $outingPhoto): static
    {
        if (!$this->outingPhotos->contains($outingPhoto)) {
            $this->outingPhotos->add($outingPhoto);
            $outingPhoto->setOuting($this);
        }

        return $this;
    }

    public function removeOutingPhoto(OutingPhoto $outingPhoto): static
    {
        if ($this->outingPhotos->removeElement($outingPhoto)) {
            // set the owning side to null (unless already changed)
            if ($outingPhoto->getOuting() === $this) {
                $outingPhoto->setOuting(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): static
    {
        if (!$this->participants->contains($participant) && $this->getParticipants()->count() < $this->getMaxRegistrations()) {
            $this->participants->add($participant);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): static
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function getOrganizer(): ?Participant
    {
        return $this->organizer;
    }

    public function setOrganizer(?Participant $organizer): static
    {
        $this->organizer = $organizer;

        $this->addParticipant($this->organizer);

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): static
    {
        $this->site = $site;

        return $this;
    }

    public function wantedState(){

        if($this->getState()->getName() == StateRepository::STATE_CREATE || $this->getState()->getName() == StateRepository::STATE_CANCEL) return null;


        $stateOpen = StateRepository::STATE_OPEN;
        $stateClosed = StateRepository::STATE_CLOSE;
        $stateInProgress = StateRepository::STATE_IN_PROGRESS;
        $statePast = StateRepository::STATE_PAST;

        $today = new \DateTime();

        $currentStateName = $this->getState()->getName();

        return match (true){
            $currentStateName == $stateOpen && ($this->getParticipants()->count() >= $this->getMaxRegistrations() || $this->getClosingDate() >= $today)=> $stateClosed,
            $currentStateName == $stateClosed && $this->getParticipants()->count() < $this->getMaxRegistrations() && $this->getClosingDate() > $today => $stateOpen,
            ($currentStateName == $stateClosed || $currentStateName == $stateOpen) && $this->getStartDate() <= $today && $this->getEndDate() >= $today => $stateInProgress,
            ($currentStateName == $stateClosed || $currentStateName == $stateOpen || $currentStateName == $stateInProgress) && $this->getEndDate() < $today => $statePast,
            default => null
        };

    }

    private function getEndDate(){
        return $this->getStartDate()->add(new DateInterval('PT' . $this->getDuration() . 'M'));
    }

    public function getCancelReason(): ?string
    {
        return $this->cancelReason;
    }

    public function setCancelReason(?string $cancelReason): static
    {
        $this->cancelReason = $cancelReason;

        return $this;
    }



}
