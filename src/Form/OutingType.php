<?php

namespace App\Form;

use App\Entity\Location;
use App\Entity\Outing;
use App\Entity\Participant;
use App\Entity\State;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OutingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la sortie',
            ])
            ->add('startDate', DateTimeType::class, [
                'label' => 'Date de la sortie',
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Durée',
                'required' => false,
            ])
            ->add('closingDate', DateType::class, [
                'label' => 'Date limite d\'inscription',
            ])
            ->add('maxRegistrations', IntegerType::class, [
                'label' => 'Nombre de places',
            ])
            ->add('infoDescription', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
            ])
            ->add('location', EntityType::class, [
                'class' => Location::class,
                'choice_label' => 'name',
            ]);
//            ->add('state', SubmitType::class, [
//                'attr' => [
//                    'name' => 'create',
//                ],
//                'label' => 'Enregistrer',
//            ])
//            ->add('state', SubmitType::class, [
//                'attr' => [
//                    'name' => 'open',
//                ],
//                'label' => 'Publier la sortie',
//            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Outing::class,
        ]);
    }
}
