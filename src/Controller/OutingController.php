<?php

namespace App\Controller;

use App\Entity\Location;
use App\Entity\Outing;
use App\Exception\UnauthorizedAccessException;
use App\Form\LocationType;
use App\Form\OutingType;
use App\Repository\OutingRepository;
use App\Repository\SiteRepository;
use App\Repository\StateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/', name:'outing_')]
class OutingController extends AbstractController
{

    #[Route('/', name: 'search')]
    public function search(SiteRepository $siteRepository): Response
    {


        $sites = $siteRepository->findAll();
        return $this->render('outing/search.html.twig', [
            'controller_name' => 'OutingController',
            'sites' => $sites,
        ]);
    }

    #[Route('outing/create', name: 'create')]
    public function create(Request $request, EntityManagerInterface $entityManager, StateRepository $stateRepository): Response
    {

        $formLocation = $this->createForm(LocationType::class);

        $form = $this->createForm(OutingType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $outing = $form->getData();
            $outing->setOrganizer($this->getUser());
            $outing->setSite($this->getUser()->getSite());

            $stateCreate = $stateRepository->findOneBy(array('name'=>StateRepository::STATE_CREATE));
            $stateOpen = $stateRepository->findOneBy(array('name'=>StateRepository::STATE_OPEN));
            $state =  $request->request->get('stateOpen') != null ? $stateOpen : $stateCreate;

            $outing->setState($state);



//            $outing->setState()

            $entityManager->persist($outing);
            $entityManager->flush();

            return $this->redirectToRoute('outing_search');
        }

        return $this->render('outing/create.html.twig', [
            'controller_name' => 'OutingController',
            'form' => $form,
            'formLocation' => $formLocation,
        ]);
    }

    #[Route('outing/detail/{id}', name: 'detail')]
    public function detail(Outing $outing, OutingRepository $outingRepository): Response
    {
//        $outing = $outingRepository->find($id);

        if (!$outing || $outing->getStartDate() <=  new \DateTime('-30 days')) {
            throw new UnauthorizedAccessException();
        }

        return $this->render('outing/detail.html.twig', [
            'controller_name' => 'ProfileController',
            'outing' => $outing,
        ]);
    }

    #[Route('outing/edit/{id}', name: 'edit')]
    public function edit(Outing $outing, Request $request, EntityManagerInterface $entityManager, StateRepository $stateRepository): Response
    {
        if (!$outing || $this->getUser()->getId() != $outing->getOrganizer()->getId() || $outing->getState()->getName() != StateRepository::STATE_CREATE) {
            throw new UnauthorizedAccessException();
        }

        $formLocation = $this->createForm(LocationType::class);


        $form = $this->createForm(OutingType::class, $outing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $outing = $form->getData();
//            if ($outing->getStartDate() > $outing->getClosingDate();)

            $stateCreate = $stateRepository->findOneBy(array('name'=>StateRepository::STATE_CREATE));
            $stateOpen = $stateRepository->findOneBy(array('name'=>StateRepository::STATE_OPEN));
            $state =  $request->request->get('stateOpen') != null ? $stateOpen : $stateCreate;
            $outing->setState($state);

            $entityManager->persist($outing);
            $entityManager->flush();

            return $this->redirectToRoute('outing_detail', ['id' => $outing->getId()]);


        }

        return $this->render('outing/edit.html.twig', [
            'controller_name' => 'ProfileController',
            'outing' => $outing,
            'form' => $form,
            'formLocation' => $formLocation,
        ]);
    }

    #[Route('outing/delete/{id}', name: 'delete')]
    public function delete(Outing $outing, EntityManagerInterface $entityManager): Response
    {
        if (!$outing || $this->getUser()->getId() != $outing->getOrganizer()->getId() || $outing->getState()->getName() != StateRepository::STATE_CREATE) {
            throw new UnauthorizedAccessException();
        }

        $entityManager->remove($outing);
        $entityManager->flush();

        return $this->redirectToRoute('outing_search');
    }


    #[Route('outing/api/location/{id}', name: 'api_location')]
    public function location(Location $location,Request $request): Response
    {
//        dd($location);
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $jsonLocation = $serializer->serialize($location, 'json');
        dd($jsonLocation);
        return new JsonResponse($location->getName());
    }


}
