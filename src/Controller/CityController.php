<?php
namespace App\Controller;

use App\Entity\City;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/city/admin', name: 'app_city')]
    public function index(): Response
    {
        $cityRepository = $this->entityManager->getRepository(City::class);
        $cities = $cityRepository->findAll();

        return $this->render('city/index.html.twig', [
            'cities' => $cities,
        ]);
    }
}