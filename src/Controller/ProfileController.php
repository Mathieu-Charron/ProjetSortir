<?php

namespace App\Controller;

use App\Exception\UnauthorizedAccessException;
use App\Form\ProfileEditType;
use App\Repository\ParticipantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'app_profile')]
    public function index(): Response
    {
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }

    #[Route('/profile/{id}', name: 'app_user_profile')]
    public function userProfile($id, ParticipantRepository $participantRepository): Response
    {
        $participant = $participantRepository->find($id);

        if (!$participant) {
            return $this->redirectToRoute('outing_search');
        }

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'participant' => $participant,
        ]);
    }

    #[Route('/profile/edit/{id}', name: 'app_edit_profile')]
    public function editProfile($id, Request $request, EntityManagerInterface $entityManager, ParticipantRepository $participantRepository, UserPasswordHasherInterface $hasher): Response
    {
        $participant = $participantRepository->find($id);

        if (!$participant || $this->getUser() != $participant) {
            throw new UnauthorizedAccessException();
        }

        $form = $this->createForm(ProfileEditType::class, $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $participant = $form->getData();

            $participant->setPassword($hasher->hashPassword(
                $participant,$participant->getPassword()
            ));


            $entityManager->persist($participant);
            $entityManager->flush();

            return $this->redirectToRoute('app_user_profile', ['id' => $participant->getId()]);
        }

        return $this->render('profile/edit.html.twig', [
            'controller_name' => 'ProfileController',
            'participant' => $participant,
            'form' => $form,
        ]);
    }
}
