<?php

namespace App\Controller;

use App\Entity\Outing;
use App\Form\LocationType;
use App\Repository\LocationRepository;
use App\Repository\OutingRepository;
use App\Repository\StateRepository;
use App\Service\LocationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/', name:'api_')]
class ApiController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    private OutingRepository $outingRepository;

    public function __construct(EntityManagerInterface $entityManager, OutingRepository $outingRepository)
    {
        $this->entityManager = $entityManager;
        $this->outingRepository = $outingRepository;
    }



    #[Route('outing/dataSearch', name: 'outing_data_search')]
    public function dataSearch(Request $request): Response
    {
        $siteId = $request->query->get('siteId');
        $siteId = $siteId == "" ? null : $siteId;
        $nameContain = $request->query->get('nameContain');

        $startDate = $request->query->get('startDate');
        $startDate = \DateTime::createFromFormat('Y-m-d H:i', str_replace('T', ' ', $startDate));
        $startDate = $startDate == false ? null : $startDate;

        $endDate = $request->query->get('endDate');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i', str_replace('T', ' ', $endDate));
        $endDate = $endDate == false ? null : $endDate;

        $iAmOrganizer = !($request->query->get('iAmOrganizer') == "false");
        $iAmRegistered = !($request->query->get('iAmRegistered') == "false");
        $iAmNotRegistered = !($request->query->get('iAmNotRegistered') == "false");
        $isOutingPast = !($request->query->get('isOutingPast') == "false");

        return new JsonResponse($this->outingRepository->searchOuting($this->getUser()->getId(), $siteId, $nameContain, $startDate, $endDate, $iAmOrganizer, $iAmRegistered,$iAmNotRegistered, $isOutingPast));

    }

    #[Route('outing/register/{id}', name: 'outing_register')]
    public function register(Outing $outing, StateRepository $stateRepository): Response
    {
        if($outing->getState()->getName() == StateRepository::STATE_OPEN){
            $outing->addParticipant($this->getUser());
            $stateNameWanted =  $outing->wantedState();
            $stateWanted = $stateRepository->findOneBy(array("name" => $stateNameWanted));
            $outing->setState($stateWanted);
            $this->entityManager->persist($outing);
            $this->entityManager->flush();
            return new JsonResponse('success', 200);

        }
        return new JsonResponse('error', 401);

    }

    #[Route('outing/unregister/{id}', name: 'outing_unregister')]
    public function unregister(Outing $outing, StateRepository $stateRepository): Response
    {
        if($outing->getState()->getName() == StateRepository::STATE_CLOSE && $outing->getParticipants()->contains($this->getUser())){
            $outing->removeParticipant($this->getUser());
            $stateNameWanted =  $outing->wantedState();
            $stateWanted = $stateRepository->findOneBy(array("name" => $stateNameWanted));
            $outing->setState($stateWanted);
            $this->entityManager->persist($outing);
            $this->entityManager->flush();
            return new JsonResponse('success', 200);

        }
        return new JsonResponse('error', 401);

    }

    #[Route('outing/publish/{id}', name: 'outing_publish')]
    public function publish(Outing $outing, StateRepository $stateRepository): Response
    {
        if($outing->getState()->getName() == StateRepository::STATE_CREATE && $outing->getOrganizer() == $this->getUser()){

            if(new \DateTime() > $outing->getClosingDate()){
                return new JsonResponse('La date de cloturation est inferieure à la date du jour', 401);
            }

            $stateOpen = $stateRepository->findOneBy(array("name" => StateRepository::STATE_OPEN));

            $outing->setState($stateOpen);
            $this->entityManager->persist($outing);
            $this->entityManager->flush();
            return new JsonResponse('success', 200);

        }
        return new JsonResponse('T\'y a pas le droit mon chou', 401);

    }




    #[Route('outing/cancel/{id}/{reason}', name: 'outing_cancel')]
    public function cancel(Outing $outing,string $reason, StateRepository $stateRepository): Response
    {
        if(($outing->getState()->getName() == StateRepository::STATE_CLOSE || $outing->getState()->getName() == StateRepository::STATE_OPEN || !empty($reason))
            && $outing->getOrganizer() == $this->getUser()){
            $stateCancel = $stateRepository->findOneBy(array("name" => StateRepository::STATE_CANCEL));
            $outing->setState($stateCancel);
            $outing->setCancelReason($reason);
            $this->entityManager->persist($outing);
            $this->entityManager->flush();
            return new JsonResponse('success', 200);

        }
        return new JsonResponse('error', 401);

    }

    #[Route('location/find/{id}', name: 'location_find')]
    public function findLocation(int $id, LocationRepository $locationRepository): Response
    {
        return new JsonResponse($locationRepository->locationWithCity($id));
    }



    #[Route('location/new', name: 'location_new')]
    public function newLocation(Request $request, LocationService $locationService): Response
    {
        $form = $this->createForm(LocationType::class);
        $form->handleRequest($request);


        return $locationService->handleFormData($form);

    }



}