<?php
namespace App\Controller;

use App\Entity\Site;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/site/admin', name: 'app_site')]
    public function index(): Response
    {
        $siteRepository = $this->entityManager->getRepository(Site::class);
        $sites = $siteRepository->findAll();

        return $this->render('site/index.html.twig', [
            'sites' => $sites,
        ]);
    }
}