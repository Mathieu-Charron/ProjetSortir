<?php

namespace App\DataFixtures;

use App\Entity\Site;
use App\Entity\State;
use App\Repository\StateRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class StateFixtures extends Fixture
{

    public const REF = "state";

//    public static int $NUMBER_GENERATE;



    public function load(ObjectManager $manager)
    {

        $stateArray = array(
            StateRepository::STATE_CREATE,
            StateRepository::STATE_OPEN,
            StateRepository::STATE_CLOSE,
            StateRepository::STATE_IN_PROGRESS,
            StateRepository::STATE_PAST,
            StateRepository::STATE_CANCEL,

            );
//        $NUMBER_GENERATE = sizeof($stateArray);
        foreach ($stateArray as $stateName){
            $state = new State();
            $state->setName($stateName);
            $manager->persist($state);
            $this->addReference(self::REF.$stateName, $state);
        }

        $manager->flush();
    }
}


