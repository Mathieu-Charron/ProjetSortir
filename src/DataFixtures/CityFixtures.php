<?php

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


use Faker\Factory;

class CityFixtures extends Fixture
{

    public const REF = "city";
    public const NUMBER_GENERATE = 25;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        for ($i = 0; $i < $this::NUMBER_GENERATE; $i++){
            $city = new City();
            $city->setName($faker->city)->setPostalCode($faker->postcode);
            $manager->persist($city);
            $this->addReference(self::REF.$i, $city);
        }

        $manager->flush();
    }
}


