<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Location;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LocationFixtures extends Fixture
{

    public const REF = "location";
    public const NUMBER_GENERATE = 25;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");



        for ($i = 0; $i < $this::NUMBER_GENERATE; $i++){
            $location = new Location();
            $location->setName("Lieu secret ".$i+1)
                ->setStreet($faker->streetAddress)
                ->setLatitude($faker->latitude)
                ->setLongitude($faker->longitude)
                ->setCity( $this->getReference( CityFixtures::REF.$faker->numberBetween(0,CityFixtures::NUMBER_GENERATE-1)));
            $manager->persist($location);
            $this->addReference(self::REF.$i, $location);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CityFixtures::class,
        ];
    }
}