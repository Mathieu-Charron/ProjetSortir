<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use App\Entity\Site;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use Faker\Factory;

class ParticipantFixtures extends Fixture implements DependentFixtureInterface
{

    private $hasher;
//    private $container;
    public const REF = "participant";
    public const NUMBER_GENERATE = 25;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        for ($i = 0; $i < $this::NUMBER_GENERATE; $i++) {
            $participant = new Participant();
            $participant->setRoles($faker->randomElements(array("ROLE_ADMIN","ROLE_USER"),1))
                        ->setFirstName($faker->firstName)
                        ->setLastName($faker->lastName)
                        ->setUsername( $participant->getFirstName().".".$participant->getLastName())
                        ->setEmail($this->str_to_noaccent(strtolower($participant->getUsername()))."@emaildefou.com")
                        ->setTelephone($faker->phoneNumber)
                        ->setActive($faker->boolean())
                        ->setPhotoUrl($faker->randomElement(array("penaldo.jpg","pessi.jpg","feur.png")))
                        ->setPassword($this->hasher->hashPassword(
                            $participant,
                            "caca"
                        )) // Remplacez 'password' par le mot de passe souhaité
                        ->setSite($this->getReference(SiteFixtures::REF.$faker->numberBetween(0,SiteFixtures::NUMBER_GENERATE-1)));

            $manager->persist($participant);
            $this->addReference(self::REF.$i, $participant);

        }

        $manager->flush();
    }

    private function str_to_noaccent($str): string
    {
        $url = $str;
        $url = preg_replace('#Ç#', 'C', $url);
        $url = preg_replace('#ç#', 'c', $url);
        $url = preg_replace('#è|é|ê|ë#', 'e', $url);
        $url = preg_replace('#È|É|Ê|Ë#', 'E', $url);
        $url = preg_replace('#à|á|â|ã|ä|å#', 'a', $url);
        $url = preg_replace('#@|À|Á|Â|Ã|Ä|Å#', 'A', $url);
        $url = preg_replace('#ì|í|î|ï#', 'i', $url);
        $url = preg_replace('#Ì|Í|Î|Ï#', 'I', $url);
        $url = preg_replace('#ð|ò|ó|ô|õ|ö#', 'o', $url);
        $url = preg_replace('#Ò|Ó|Ô|Õ|Ö#', 'O', $url);
        $url = preg_replace('#ù|ú|û|ü#', 'u', $url);
        $url = preg_replace('#Ù|Ú|Û|Ü#', 'U', $url);
        $url = preg_replace('#ý|ÿ#', 'y', $url);
        $url = preg_replace('#Ý#', 'Y', $url);
        return ($url);
    }

    public function getDependencies()
    {
        return [
            SiteFixtures::class,
        ];
    }

}


