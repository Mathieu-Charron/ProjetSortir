<?php

namespace App\DataFixtures;

use App\Entity\Outing;
use App\Entity\OutingPhoto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Finder\Finder;

class OutingPhotoFixtures extends Fixture implements dependentFixtureInterface
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        $iOuting=0;

        $pathImg = explode('src',__DIR__)[0].'public\img\outing\fixtures\\';

//        $imgDirectory = $finder->in($pathImg."Ptit ping pong");

//        dd($imgDirectory->files()->name("Ptit ping pong"."1"));

        foreach (OutingFixtures::ACTIVITIES as $activity) {
            $finder = new Finder();
            $imgDirectory = $finder->in($pathImg.$activity);
            $imgCount = $imgDirectory->files()->count();

//            dd($imgDirectory->files()->name($activity));
            foreach ($imgDirectory as $file){
                $photo = new OutingPhoto();
                $photo->setFileName($file->getFilename());
                $photo->setOuting($this->getReference(OutingFixtures::REF.$iOuting));

                $manager->persist($photo);
            }
            $iOuting++;

        }

        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            OutingFixtures::class,
        ];
    }
}