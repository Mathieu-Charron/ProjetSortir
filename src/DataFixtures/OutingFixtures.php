<?php

namespace App\DataFixtures;

use App\Entity\Outing;
use App\Entity\Site;
use App\Entity\State;
use App\Repository\StateRepository;
use Couchbase\UserManager;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Finder\Finder;

class OutingFixtures extends Fixture implements DependentFixtureInterface
{
    public const ACTIVITIES = array(
            "Ptit ping pong",
            "Ptit Five classico",
            "Accrobranche",
            "Championnat du monde du silence",
            "Chasse aux objets invisibles",
            "Prix de la meilleure coupe",
            "Concours de danse",
            "Aftermatch",
            "Visite au musée",
            "Visite guidée au cimetière"
    );

    public const REF = "outing";
    public const NUMBER_GENERATE = 10;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");


        $iOuting=0;
        foreach ($this::ACTIVITIES as $activity) {

            $nbrUsers = $faker->numberBetween(1, 8);
            $organizer = $this->getReference(ParticipantFixtures::REF . $faker->numberBetween(0, ParticipantFixtures::NUMBER_GENERATE-1));

            $allUsers = array();
            for ($i = 0; $i < ParticipantFixtures::NUMBER_GENERATE - 1; $i++) {
                $user = $this->getReference(ParticipantFixtures::REF . $i);
                if($user != $organizer){
                    $allUsers[] = $user;
                }
            }

            $usersInOuting = array();
            if($nbrUsers == 1){
                $usersInOuting = array($organizer);
            } else {
                $usersInOuting = $faker->randomElements($allUsers, $faker->numberBetween(1, $nbrUsers - 1));
                $usersInOuting[] = $organizer;
            }

            $closingDate = $faker->dateTimeBetween('-30 days', '+30 days');

            $startDate = $faker->dateTimeBetween($closingDate, '+30 days');
            $duration = $this->roundToMultiple30($faker->numberBetween(30, 240));
            $endActivityDate = $startDate->add(new DateInterval('PT' . $duration . 'M'));

            $currentDate = new DateTime('now');
            $stateString = match (true){
                $currentDate < $closingDate &&  sizeof($usersInOuting) < $nbrUsers
                    => $faker->randomElement(array(StateRepository::STATE_CREATE, StateRepository::STATE_OPEN, StateRepository::STATE_CANCEL)),
                ($currentDate < $closingDate  &&  sizeof($usersInOuting) == $nbrUsers) || $this->dateIsBetween($currentDate, $startDate, $closingDate)
                    => $faker->randomElement(array(StateRepository::STATE_CLOSE, StateRepository::STATE_CANCEL)),
                $this->dateIsBetween($currentDate, $startDate, $endActivityDate)
                    => StateRepository::STATE_IN_PROGRESS,
                $currentDate > $endActivityDate
                    => StateRepository::STATE_PAST,
            };

            $outing = new Outing();



            $outing->setName($activity)
                ->setLocation($this->getReference(LocationFixtures::REF . $faker->numberBetween(0, LocationFixtures::NUMBER_GENERATE - 1)))
                ->setOrganizer($organizer)
                ->setDuration($duration)
                ->setSite($organizer->getSite())
                ->setClosingDate($closingDate)
                ->setInfoDescription($faker->paragraph(3))
                ->setMaxRegistrations($nbrUsers)
                ->setStartDate($startDate)
                ->setState($this->getReference(StateFixtures::REF.$stateString));

            foreach ($usersInOuting as $user) {
                $outing->addParticipant($user);
            }

            $manager->persist($outing);
            $this->addReference(self::REF.$iOuting++,$outing);
        }
        $manager->flush();

    }


    private function roundToMultiple30(int $number): int
    {
        return round($number / 30) * 30;
    }

    private function dateIsBetween(DateTime $date, DateTime $dateStart, DateTime $dateEnd): bool
    {
        return $date <= $dateStart && $date >= $dateEnd;
    }

    public function getDependencies()
    {
        return [
            ParticipantFixtures::class,
            StateFixtures::class,
            LocationFixtures::class,
            SiteFixtures::class
        ];
    }
}