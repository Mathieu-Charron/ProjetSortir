<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use App\Entity\Site;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Faker\Factory;

class SiteFixtures extends Fixture
{

    public const REF = "site";
    public const NUMBER_GENERATE = 4;


    public function load(ObjectManager $manager)
    {

        $siteStringArray = array("SAINT HERBALIN", "RENNES", "NIORT", "QUIMPER");
        $i = 0;
        foreach ($siteStringArray as $siteName){
            $site = new Site();
            $site->setName($siteName);
            $manager->persist($site);
            $this->addReference(self::REF.$i++, $site);

        }

        $manager->flush();
    }
}


