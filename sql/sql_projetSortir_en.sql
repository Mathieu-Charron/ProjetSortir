CREATE TABLE STATES (
    state_id   INTEGER NOT NULL,
    label      VARCHAR(30) NOT NULL
);

ALTER TABLE STATES ADD CONSTRAINT states_pk PRIMARY KEY (state_id);

CREATE TABLE REGISTRATIONS (
    registration_date            smalldatetime NOT NULL,
    outing_id                   INTEGER NOT NULL,
    participant_id              INTEGER NOT NULL
);

ALTER TABLE REGISTRATIONS ADD CONSTRAINT registrations_pk PRIMARY KEY (outing_id, participant_id);

CREATE TABLE LOCATIONS (
    location_id         INTEGER NOT NULL,
    location_name       VARCHAR(30) NOT NULL,
    street              VARCHAR(30),
    latitude            FLOAT,
    longitude           FLOAT,
    city_id             INTEGER NOT NULL
);

ALTER TABLE LOCATIONS ADD CONSTRAINT locations_pk PRIMARY KEY (location_id);

CREATE TABLE PARTICIPANTS (
    participant_id      INTEGER NOT NULL,
    username            VARCHAR(30) NOT NULL,
    last_name           VARCHAR(30) NOT NULL,
    first_name          VARCHAR(30) NOT NULL,
    telephone           VARCHAR(15),
    email               VARCHAR(20) NOT NULL,
    password            VARCHAR(20) NOT NULL,
    photo_url           VARCHAR(500),
    administrator       bit NOT NULL,
    active              bit NOT NULL,
    site_id             INTEGER NOT NULL
);

ALTER TABLE PARTICIPANTS ADD CONSTRAINT participants_pk PRIMARY KEY (participant_id);

ALTER TABLE PARTICIPANTS ADD CONSTRAINT participants_username_uk UNIQUE (username);

CREATE TABLE SITES (
    site_id         INTEGER NOT NULL,
    site_name       VARCHAR(30) NOT NULL
);

ALTER TABLE SITES ADD CONSTRAINT sites_pk PRIMARY KEY (site_id);

CREATE TABLE OUTINGS (
    outing_id                     INTEGER NOT NULL,
    name                          VARCHAR(30) NOT NULL,
    start_date                    datetime NOT NULL,
    duration                      INTEGER,
    closing_date                  date NOT NULL,
    max_registrations             INTEGER NOT NULL,
    info_description              VARCHAR(500),
    outing_state                  INTEGER,
    photo_url                     VARCHAR(500),
    organizer                     INTEGER NOT NULL,
    location_id                   INTEGER NOT NULL,
    state_id                      INTEGER NOT NULL
);

ALTER TABLE OUTINGS ADD CONSTRAINT outings_pk PRIMARY KEY (outing_id);

CREATE TABLE CITIES (
    city_id         INTEGER NOT NULL,
    city_name       VARCHAR(30) NOT NULL,
    postal_code     VARCHAR(10) NOT NULL
);

ALTER TABLE CITIES ADD CONSTRAINT cities_pk PRIMARY KEY (city_id);

ALTER TABLE REGISTRATIONS
    ADD CONSTRAINT registrations_participants_fk FOREIGN KEY (participant_id)
        REFERENCES participants (participant_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;

ALTER TABLE REGISTRATIONS
    ADD CONSTRAINT registrations_outings_fk FOREIGN KEY (outing_id)
        REFERENCES outings (outing_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;

ALTER TABLE LOCATIONS
    ADD CONSTRAINT locations_cities_fk FOREIGN KEY (city_id)
        REFERENCES cities (city_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;
    
ALTER TABLE OUTINGS
    ADD CONSTRAINT outings_states_fk FOREIGN KEY (state_id)
        REFERENCES states (state_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;

ALTER TABLE OUTINGS
    ADD CONSTRAINT outings_locations_fk FOREIGN KEY (location_id)
        REFERENCES locations (location_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;

ALTER TABLE OUTINGS
    ADD CONSTRAINT outings_organizers_fk FOREIGN KEY (organizer)
        REFERENCES participants (participant_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;

-- Added to the script
ALTER TABLE PARTICIPANTS
    ADD CONSTRAINT participants_sites_fk FOREIGN KEY (site_id)
        REFERENCES sites (site_id)
ON DELETE NO ACTION 
    ON UPDATE NO ACTION;
